#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSizePolicy>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->spinBox->setMaximum(QApplication::desktop()->screenCount());

    screenCount=QApplication::desktop()->screenCount();
    viewMonitor=-1;
    refreshtime = 40;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(prntScr()));
    timer->start(refreshtime);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::prntScr()
{
    QPixmap okno;
        if(screenCount>1 && viewMonitor>0){

            int width1Monitor = QApplication::desktop()->screen(-1)->width();
            int width2Monitor =QApplication::desktop()->screen(viewMonitor)->width();
            int height2Monitor = QApplication::desktop()->screen(viewMonitor)->height();

            okno = QPixmap::grabWindow(QApplication::desktop()->winId(),width1Monitor,0,width2Monitor, height2Monitor);
        }else{
            okno=QPixmap::grabWindow(QApplication::desktop()->winId());
        }

        ui->label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        ui->label->setScaledContents(true);
        ui->label->clear();
        ui->label->setPixmap(okno);


}

void MainWindow::on_pushButton_clicked()
{

    timer->stop();
    viewMonitor = ui->spinBox->value()-1;
    timer->start(refreshtime);

}
