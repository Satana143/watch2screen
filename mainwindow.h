#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QDesktopWidget>
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();     
     QTimer * timer;
     int screenCount;
     int viewMonitor;
     int refreshtime;

private slots:
    void on_pushButton_clicked();
public slots:
    void prntScr();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
